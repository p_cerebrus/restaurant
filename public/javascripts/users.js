var users = angular.module('users', ['ui.bootstrap']);

users.controller('usersCtrl', ['$scope', '$http', '$modal', function ($scope, $http, $modal){

	$scope.newUser = function(){
	};

	$scope.elevated = [
		{username:'admin', email: 'admin@admin.com'},
		{username:'admin2', email: 'admin2@admin.com'}
	];

	$scope.general = [
		{username:'test', email: 'test@test.com'},
		{username:'test2', email: 'test2@test.com'},
		{username:'test3', email: 'test3@test.com'}
	];

	$scope.newUser = function(){
		var modalInstance = $modal.open({
			templateUrl: 'newUserModal.html',
			controller: 'newUserModalCtrl'
		});
	};

}]);

users.controller('newUserModalCtrl',function($scope, $modalInstance){
	$scope.cancel = function(){
		$modalInstance.close();
	};
});
