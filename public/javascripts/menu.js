var menu = angular.module('menu', ['ui.bootstrap', 'ngAside']);

menu.controller('menuCtrl', ['$scope', '$http', '$modal', '$aside', function ($scope, $http, $modal, $aside){

	$scope.menuItems = [
		{name: 'Food 1', details: 'It\'s really good.', price: '9.99'},
		{name: 'Food 2', details: 'It\'s really good.', price: '9.99'},
		{name: 'Food 3', details: 'It\'s really good.', price: '9.99'},
		{name: 'Food 4', details: 'It\'s really good.', price: '9.99'},
		{name: 'Food 5', details: 'It\'s really good.', price: '9.99'},
		{name: 'Food 6', details: 'It\'s really good.', price: '9.99'},
		{name: 'Food 7', details: 'It\'s really good.', price: '9.99'},
		{name: 'Food 8', details: 'It\'s really good.', price: '9.99'},
		{name: 'Food 9', details: 'It\'s really good.', price: '9.99'}
	];

	$scope.orderedItems = new Array();

	$scope.selectItem = function(selectedItem){
		var modalInstance = $modal.open({
			templateUrl: 'itemModal.html',
			controller: 'itemModalCtrl',
			resolve: {
				items: function(){
					return {
						selectedItem: selectedItem,
						orderedItems: $scope.orderedItems
					};
				}
			}
		});
	};

	$scope.openAside = function(){
		var modalInstance = $aside.open({
			templateUrl: 'aside.html',
			controller: 'asideCtrl',
			placement: 'right',
			backdrop: true,
			resolve: {
				items: function(){
					return {
						orderedItems: $scope.orderedItems
					};
				}
			}
		});
	};
}]);

menu.controller('itemModalCtrl',function($scope, $modalInstance, items){

	$scope.item = items.selectedItem;
	$scope.order = items.orderedItems;

	$scope.cash = function(){
		$scope.order.push({
			item: $scope.item,
			payment: "cash"
		});
		$modalInstance.close();
	};

	$scope.credit = function(){
		$scope.order.push({
			item: $scope.item,
			payment: "credit"
		});
		$modalInstance.close();
	};

	$scope.check = function(){
		$scope.order.push({
			item: $scope.item,
			payment: "check"
		});
		$modalInstance.close();
	};

	$scope.cancel = function(){
		$modalInstance.close();
	};
});

menu.controller('asideCtrl', function($scope, $modalInstance, items){

	$scope.order = items.orderedItems;

	$scope.remove = function(index){
		$scope.order.splice(index, 1);
	};

	$scope.submit = function(e) {
		window.location.href = '';
		$modalInstance.close();
		e.stopPropagation();
	};

	$scope.cancel = function(e) {
		$modalInstance.close();
		e.stopPropagation();
	};
});

menu.controller('paymentCtrl',function($scope, $modalInstance){
	$scope.cancel = function(){
		$modalInstance.close();
	};
});