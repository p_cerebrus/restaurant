function pieChart(chartInfo){
	var chart;
	var legend;

	var chartData = chartInfo.data;

	AmCharts.ready(function () {
		chart = new AmCharts.AmPieChart();
		chart.dataProvider = chartData;
		chart.titleField = chartInfo.titleField;
		chart.valueField = chartInfo.valueField;
		chart.outlineColor = "#FFFFFF";
		chart.outlineAlpha = 0.8;
		chart.outlineThickness = 2;

		// config
		chart.amExport = {
			top      : 0,
			right    : 0,
			exportJPG: true,
			exportPNG: true,
			exportSVG: true
		}

		chart.amExport.userCFG = {
			menuTop   : 'auto',
			menuLeft  : 'auto',
			menuRight : '0px',
			menuBottom: '0px',
			menuItems : [{
				textAlign: 'center',
				icon     : 'javascripts/amcharts/amcharts/images/export.png',
				iconTitle: 'Save chart as an image',
				onclick  : function(){},
				items    : [{
					title: 'JPG',
					format: 'jpg'
				}, {
					title: 'PNG',
					format: 'png'
				}, {
					title: 'SVG',
					format: 'svg'
				}, {
					title: 'PDF',
					format: 'pdf'
				}]
			}],
			menuItemStyle: {
				backgroundColor        : 'transparent',
				opacity                : 1,
				rollOverBackgroundColor: '#EFEFEF',
				color                  : '#000000',
				rollOverColor          : '#CC0000',
				paddingTop             : '6px',
				paddingRight           : '6px',
				paddingBottom          : '6px',
				paddingLeft            : '6px',
				marginTop              : '0px',
				marginRight            : '0px',
				marginBottom           : '0px',
				marginLeft             : '0px',
				textAlign              : 'left',
				textDecoration         : 'none',
				fontFamily             : 'Arial', // Default: charts default
				fontSize               : '12px',  // Default: charts default
			},
			menuItemOutput: {
				backgroundColor: '#FFFFFF',
				fileName       : 'amCharts',
				format         : 'png',
				output         : 'dataurlnewwindow',
				render         : 'browser',
				dpi            : 90,
				onclick        : function(instance, config, event){
					event.preventDefault();
					instance.output(config);
				}
			},
			legendPosition: "bottom",    //top,left,right
			removeImagery : true
		}

		chart.write(chartInfo.divName);		
	});
}

function timeline(chartInfo){
	var chart = AmCharts.makeChart(chartInfo.divName, {
		"type": "serial",
		"theme": "none",
		"pathToImages": "http://www.amcharts.com/lib/3/images/",
		"dataDateFormat": "YYYY-MM-DD",
		"valueAxes": [{
			"id":"v1",
			"axisAlpha": 0,
			"position": "left"
		}],
		"graphs": [{
			"id": "g1",
			"bullet": "round",
			"bulletBorderAlpha": 1,
			"bulletColor": "#FFFFFF",
			"bulletSize": 5,
			"hideBulletsCount": 50,
			"lineThickness": 2,
			"title": "red line",
			"useLineColorForBulletBorder": true,
			"valueField": "value"
		}],
		"chartScrollbar": {
			"graph": "g1",
			"scrollbarHeight": 30
		},
		"chartCursor": {
			"cursorPosition": "mouse",
			"pan": true,
			 "valueLineEnabled":true,
			 "valueLineBalloonEnabled":true
		},
		"categoryField": "date",
		"categoryAxis": {
			"parseDates": true,
			"dashLength": 1,
			"minorGridEnabled": true,
			"position": "top"
		},
		exportConfig:{
		  menuRight: '20px',
		  menuBottom: '50px',
		  menuItems: [{
		  icon: 'http://www.amcharts.com/lib/3/images/export.png',
		  format: 'png'	  
		  }]  
		},
		"dataProvider": chartInfo.data
	});

	chart.addListener("rendered", zoomChart);

	zoomChart();
	function zoomChart(){
		chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);
	}
}