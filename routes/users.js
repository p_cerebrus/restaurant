"use strict";

var bcrypt = require('bcrypt'),
	joi = require('joi'),
    connect = require('../connect.js');

module.exports = {
	usersPage: function(req, res){
		res.render('users');
	},
	loginPage: function(req, res){
		res.render('login', {error: false});
	},
	login: function(req, res){
		validate(req.body.username, req.body.password, function(error){
			if (error){
				res.render('login', { error: true });
			}
			else {
				req.session.username = req.body.username;

				//remember me
				if (req.body.cookie){
					req.session.cookie.maxAge = 1000*60*60*24*30    //30 days
				}

				res.redirect('/');
			}
		});
	},
	logout: function(req, res){
		req.session.destroy(function(error){
			if(error){
				console.log(error);
			}
			res.redirect('/login');
		});
	},
	checkUser: function(req, res, next){
		if (!req.session.username){
			res.redirect('/login');
		}
		else {
			next();
		}
	}
};

function validate(username, password, callback){
	connect.dbQuery('SELECT password FROM users WHERE username=$1', [username], function(rows){
		//user exists
		if (rows.length > 0){
			bcrypt.compare(password, rows[0].password, function(error, result){
				//password ok
				if (result){
					callback(false);
				}
				//bad password
				else {
					callback(true);
				}
			});
		}
		//user does not exist
		else {
			callback(true);
		}
	});
}