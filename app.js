"use strict";

// --------- Packages ---------
var express = require('express'),
    path = require('path'),
    favicon = require('serve-favicon'),
    logger = require('morgan'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    session = require('express-session');




// ------ Route Modules -------
var dashboard = require('./routes/dashboard'),
    menu = require('./routes/menu'),
    users = require('./routes/users');





// ---------- Setup -----------
// └─┬ app
var app = express();
//     ├ views
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
//     ├ public folder
app.use(express.static(path.join(__dirname, 'public')));
//     ├ favicon location
// app.use(favicon(path.join(__dirname, 'public', 'images', 'favicon.ico')));
//     ├ log string format
app.use(logger(':remote-addr   :response-time ms   :status :method :url'));
//     ├ body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//     ├ cookie parser
app.use(cookieParser());
//     └ session cookie
app.use(session({
	secret: 'S3CR37PV55W0RD',
	resave: false,
	saveUninitialized: true
}));




// ---------- Routes ----------

app.route('/')
	// .all(users.checkUser)
	.get(dashboard.dashboardPage);

app.route('/menu')
	// .all(users.checkUser)
	.get(menu.menuPage);

app.route('/users')
	// .all(users.checkUser)
	.get(users.usersPage);

app.route('/login')
	.get(users.loginPage)
	.post(users.login);

app.route('/logout')
	// .all(users.checkUser)
	.get(users.logout);

// default route
app.use(function(req, res){
	res.status(err.status || 500);
	res.render('error');
});

// return error instead of stack trace
app.use(function(err, req, res, next){
	res.status(err.status || 500);
	res.render('error');
});




// ---------- Server ----------
app.listen(3000);
console.log('Server listening on port 3000');