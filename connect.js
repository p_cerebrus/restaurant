"use strict";

var credentials = require('./credentials.js'),
    db = require('pg');

module.exports = {
	dbQuery: function(queryString, queryParams, callback){
		db.connect(credentials.dbString, function(err, client, done){
			if(err) {
				return console.error('error fetching client from pool', err);
			}
			client.query(queryString, queryParams, function(err, result){
				done();
				if(err) {
					return console.error('error running query', err);
				}
				callback(result.rows);
			});
		});
	}
}